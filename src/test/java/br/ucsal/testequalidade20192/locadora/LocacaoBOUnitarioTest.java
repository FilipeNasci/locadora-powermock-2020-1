package br.ucsal.testequalidade20192.locadora;

import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import br.ucsal.testequalidade20192.locadora.business.LocacaoBO;
import br.ucsal.testequalidade20192.locadora.dominio.Cliente;
import br.ucsal.testequalidade20192.locadora.dominio.Locacao;
import br.ucsal.testequalidade20192.locadora.dominio.Modelo;
import br.ucsal.testequalidade20192.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20192.locadora.persistence.ClienteDAO;
import br.ucsal.testequalidade20192.locadora.persistence.LocacaoDAO;
import br.ucsal.testequalidade20192.locadora.persistence.VeiculoDAO;


@RunWith(PowerMockRunner.class)
@PrepareForTest({ ClienteDAO.class, VeiculoDAO.class })
public class LocacaoBOUnitarioTest {

	/**
	 * Verificar se ao locar um ve�culo dispon�vel para um cliente cadastrado, um
	 * contrato de loca��o � inserido.
	 * 
	 * M�todo:
	 * 
	 * public static Integer locarVeiculos(String cpfCliente, List<String> placas,
	 * Date dataLocacao, Integer quantidadeDiasLocacao) throws
	 * ClienteNaoEncontradoException, VeiculoNaoEncontradoException,
	 * VeiculoNaoDisponivelException, CampoObrigatorioNaoInformado
	 *
	 * Observa��o1: lembre-se de mocar os m�todos necess�rios nas classes
	 * ClienteDAO, VeiculoDAO e LocacaoDAO.
	 * 
	 * Observa��o2: lembre-se de que o m�todo locarVeiculos � um m�todo command.
	 * 
	 * @throws Exception
	 */


	@Test
	public void locarClienteCadastradoUmVeiculoDisponivel() throws Exception {
		Cliente cliente = new Cliente("1", "Fulano", "3434-3434");
		Modelo ferrari = new Modelo("Ferrari");
		Veiculo veiculo = new Veiculo("4", 2020, ferrari, 50d);	
		
		List<Veiculo> veiculos = new ArrayList<>();
		veiculos.add(veiculo);
		List<String> placas = new ArrayList<>();
		String placa = "4";
		placas.add(placa);
		
		Date hoje = new Date();
		Locacao locacao = new Locacao(cliente, veiculos, hoje, 1);
		
		LocacaoBO lbo = new LocacaoBO();
		LocacaoBO spy1 = spy(lbo);
		
		LocacaoDAO ldao = new LocacaoDAO();
		LocacaoDAO spy2 = spy(ldao);
		
		mockStatic(ClienteDAO.class);
		when(ClienteDAO.obterPorCpf("1")).thenReturn(cliente);
		
		mockStatic(VeiculoDAO.class);
		when(VeiculoDAO.obterPorPlaca("4")).thenReturn(veiculo);
		
		LocacaoBO.locarVeiculos("1", placas, hoje, 1);
		spy2.insert(locacao);
	}
}
